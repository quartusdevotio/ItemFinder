using System;
using System.Collections.Generic;
using UnityEngine;

namespace ItemFinder.Map
{
    public class MapController : MonoBehaviour
    {
        [SerializeField] private MapTile prefab;
        [SerializeField] private Vector2 startingTilePosition;
        [SerializeField] private List<MapTile> mapTileList;

        public event Action<Vector2> TileMapPositionCatched;

        public void OnMapTileSelected(GameObject mapTileObject)
        {
            MapTile mapTile = mapTileObject.GetComponent<MapTile>();

            if (!mapTile.IsActive()) return;

            Vector2 output = mapTile.GetPosition();
            TileMapPositionCatched?.Invoke(output);
        }

        public void GenerateBoard(int width, int height)
        {
            Vector2 tilePosition = startingTilePosition;

            for (int i=0; i<height; i++)
            {
                for(int j=0; j<width; j++)
                {
                    mapTileList.Add(Instantiate(prefab, tilePosition, Quaternion.identity));
                    tilePosition.x += 1;
                }
                tilePosition.y += 1;
                tilePosition.x = startingTilePosition.x;
            }
        }

        public void ResetBoard()
        {
            foreach(MapTile mapTile in mapTileList)
            {
                mapTile.Enable();
            }
        }

        public Vector2 GetStartingTilePosition()
        {
            var output = startingTilePosition;
            return output;
        }
    }
}


﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace ItemFinder.UI
{
    public class StartMenuController : MenuController
    {
        public event Action PlayPressed;
        public event Action QuitPressed;

        [SerializeField] private Button playButton;
        [SerializeField] private Button quitButton;

        private void Start()
        {
            playButton.onClick.AddListener(PlayButtonClicked);
            quitButton.onClick.AddListener(QuitButtonClicked);
        }

        private void OnDestroy()
        {
            playButton.onClick.RemoveListener(PlayButtonClicked);
            quitButton.onClick.RemoveListener(QuitButtonClicked);
        }

        private void PlayButtonClicked()
        {
            Hide();
            PlayPressed?.Invoke();
        }

        private void QuitButtonClicked()
        {
            QuitPressed?.Invoke();
        }
    }
}
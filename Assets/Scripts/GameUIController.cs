using ItemFinder.Manager;
using TMPro;
using UnityEngine;

namespace ItemFinder.UI
{
    public class GameUIController : MonoBehaviour
    {
        [SerializeField] private GameManager gameManager;
        [SerializeField] private TMP_Text timerText;
        [SerializeField] private TMP_Text scoreText;
        [SerializeField] private Canvas canvas;

        private void Start()
        {
            gameManager.TimerUpdated += OnTimerUpdated;
            gameManager.ScoreUpdated += OnScoreUpdated;
        }

        private void OnTimerUpdated(float timer)
        {
            if(timer <= 0)
            {
                timerText.text = "Time's Up!";
                return;
            }

            timerText.text = "Time Left: " + ((int)timer).ToString();
        }

        private void OnScoreUpdated(int score)
        {
            scoreText.text = "Score: " + score.ToString();
        }

        public void Hide()
        {
            canvas.enabled = false;
        }

        public void Show()
        {
            canvas.enabled = true;
        }
    }
}



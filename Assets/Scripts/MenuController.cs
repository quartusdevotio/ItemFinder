using UnityEngine;

namespace ItemFinder.UI
{
    public class MenuController : MonoBehaviour
    {
        [SerializeField] protected Canvas canvas;

        public void Hide()
        {
            canvas.enabled = false;
        }

        public void Show()
        {
            canvas.enabled = true;
        }

    }
}
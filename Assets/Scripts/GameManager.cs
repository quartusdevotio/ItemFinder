using ItemFinder.Map;
using ItemFinder.Player;
using ItemFinder.UI;
using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace ItemFinder.Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private PlayerController player;
        [SerializeField] private GameUIController gameUI;
        [SerializeField] private MapController map;
        [SerializeField] private GameOverPopupController gameOverPopup;
        [SerializeField] private StartMenuController startMenu;

        private int score;
        private float timer = 30f;
        private const int startingPositionOffset = 9;
        private const float randomizedPositionOffset = 0.5f;
        private const int boardWidth = 10;
        private const int boardHeight = 10;
        private bool timerStart = false;

        private Vector2 itemPosition;

        public event Action<int> GameOver;
        public event Action<float> TimerUpdated;
        public event Action<int> ScoreUpdated;

        private void Start()
        {
            player.MapTileSelected += map.OnMapTileSelected;
            player.MapTileSelected += OnMapTileSelected;
            startMenu.PlayPressed += OnPlayPressed;
            startMenu.QuitPressed += OnQuitPressed;
            gameOverPopup.PlayAgainPressed += OnPlayAgainPressed;
            gameOverPopup.QuitPressed += OnQuitPressed;
            GameOver += gameOverPopup.OnGameOver;
            map.TileMapPositionCatched += player.OnTilePositionCatched;

            timerStart = false;
        }

        private void OnDestroy()
        {
            player.MapTileSelected -= map.OnMapTileSelected;
            player.MapTileSelected -= OnMapTileSelected;
            startMenu.PlayPressed -= OnPlayPressed;
            startMenu.QuitPressed -= OnQuitPressed;
            gameOverPopup.PlayAgainPressed -= OnPlayAgainPressed;
            gameOverPopup.QuitPressed -= OnQuitPressed;
            map.TileMapPositionCatched -= player.OnTilePositionCatched;
        }

        private void Initialize()
        {
            timer = 30f;
            timerStart = true;
            score = 0;
            player.Activate();
            Reset();
            ResetScore();
        }

        private void Reset()
        {
            map.ResetBoard();
            RandomizeItem();
            RandomizePlayer();
        }

        private void Update()
        {
            Timer();
        }

        private void Timer()
        {
            if (!timerStart) return;
            timer -= Time.deltaTime;
            TimerUpdated?.Invoke(timer);
            if(timer <= 0)
            {
                AnnounceGameOver();
            }
        }
        
        private void AddScore()
        {
            score += 1;
            ScoreUpdated?.Invoke(score);
        }

        private void ResetScore()
        {
            score = 0;
            ScoreUpdated?.Invoke(score);
        }

        private void RandomizeItem()
        {
            itemPosition = Randomize();
        }

        private void RandomizePlayer()
        {
            player.transform.position = Randomize();

            while((Vector2)player.transform.position == itemPosition)
            {
                player.transform.position = Randomize();
            }

            RaycastHit2D hitData = Physics2D.Raycast(player.transform.position, Vector2.zero, 0, player.GetTileLayer());
            if (hitData)
            {
                OnMapTileSelected(hitData.transform.gameObject);
            }
        }

        private Vector2 Randomize()
        {
            Vector2 startingPosition = map.GetStartingTilePosition();
            Vector2 randomizedPosition = new Vector2(Random.Range((int)startingPosition.x, (int)startingPosition.x + startingPositionOffset) + randomizedPositionOffset,
                Random.Range((int)startingPosition.y, (int)startingPosition.y + startingPositionOffset) + randomizedPositionOffset);
            return randomizedPosition;
        }

        private void OnMapTileSelected(GameObject mapTileObject)
        {
            CheckTile(mapTileObject);
        }

        private void CheckTile(GameObject mapTileObject)
        {
            MapTile mapTile = mapTileObject.GetComponent<MapTile>();

            if (!mapTile.IsActive()) return;
            
            if(mapTile.IsItemOnTile(itemPosition))
            {
                Reset();
                AddScore();
                return;
            }
            FindArrowDirection();
            DisableTile(mapTile);
        }

        private void FindArrowDirection()
        {
            Vector2 direction = (itemPosition - (Vector2)player.transform.position).normalized;

            player.ResetAllArrows();
            if (direction.x < 0f)
            {
                player.ActivateArrow(Direction.Left);
            }
            else if(direction.x > 0f)
            {
                player.ActivateArrow(Direction.Right);
            }

            if (direction.y < 0f)
            {
                player.ActivateArrow(Direction.Down);
            }
            else if (direction.y > 0f)
            {
                player.ActivateArrow(Direction.Up);
            }
        }
        
        private void DisableTile(MapTile mapTile)
        {
            mapTile.Disable();
        }

        private void AnnounceGameOver()
        {
            timerStart = false;
            player.Deactivate();
            GameOver?.Invoke(score);
        }

        private void OnPlayPressed()
        {
            player.gameObject.SetActive(true);
            gameUI.Show();
            map.GenerateBoard(boardWidth, boardHeight);
            Initialize();
        }

        private void OnPlayAgainPressed()
        {
            Initialize();
        }

        private void OnQuitPressed()
        {
            Application.Quit();
        }
    }
}


using UnityEngine;

namespace ItemFinder.Map
{
    public class MapTile : MonoBehaviour
    {
        [SerializeField] private Vector2 position;
        [SerializeField] private SpriteRenderer spriteRenderer;
        [SerializeField] private Sprite enabledSprite;
        [SerializeField] private Sprite disabledSprite;
        [SerializeField] private bool active = true;

        private void Start()
        {
            position = transform.position;
        }

        public Vector2 GetPosition()
        {
            var output = position;
            return position;
        }

        public bool IsActive()
        {
            var output = active;
            return output;
        }

        public void Enable()
        {
            active = true;
            spriteRenderer.sprite = enabledSprite;
        }

        public void Disable()
        {
            active = false;
            spriteRenderer.sprite = disabledSprite;
        }

        public bool IsItemOnTile(Vector2 itemPosition)
        {
            var output = position == itemPosition ? true : false;
            return output;
        }
    }
}

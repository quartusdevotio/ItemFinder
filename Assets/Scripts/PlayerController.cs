using ItemFinder.Map;
using System;
using UnityEngine;

namespace ItemFinder.Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private Camera mainCamera;
        [SerializeField] private MapTile selectedMapTile;
        [SerializeField] private LayerMask tileLayer;

        [SerializeField] private SpriteRenderer arrowTop;
        [SerializeField] private SpriteRenderer arrowBottom;
        [SerializeField] private SpriteRenderer arrowLeft;
        [SerializeField] private SpriteRenderer arrowRight;

        public event Action<GameObject> MapTileSelected;

        private bool active = true;

        void Update()
        {
            if (!active) return;

            Vector3 worldPosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hitData = Physics2D.Raycast(new Vector2(worldPosition.x, worldPosition.y), Vector2.zero, 0, tileLayer);
            if (hitData && Input.GetMouseButtonDown(0))
            {
                MapTileSelected?.Invoke(hitData.transform.gameObject);
            }
        }

        public void OnTilePositionCatched(Vector2 targetPosition)
        {
            Move(targetPosition);
        }

        public void Move(Vector2 targetPosition)
        {
            Vector2 distance = targetPosition - (Vector2)transform.position;
            transform.position += (Vector3)distance;
        }

        public LayerMask GetTileLayer()
        {
            var output = tileLayer;
            return output;
        }

        public void ActivateArrow(Direction direction)
        {
            switch(direction)
            {
                case Direction.Up:
                    arrowTop.enabled = true;
                    break;
                case Direction.Down:
                    arrowBottom.enabled = true;
                    break;
                case Direction.Left:
                    arrowLeft.enabled = true;
                    break;
                case Direction.Right:
                    arrowRight.enabled = true;
                    break;
            }
        }

        public void DeactivateArrow(Direction direction)
        {
            switch (direction)
            {
                case Direction.Up:
                    arrowTop.enabled = false;
                    break;
                case Direction.Down:
                    arrowBottom.enabled = false;
                    break;
                case Direction.Left:
                    arrowLeft.enabled = false;
                    break;
                case Direction.Right:
                    arrowRight.enabled = false;
                    break;
            }
        }

        public void ResetAllArrows()
        {
            arrowTop.enabled = false;
            arrowBottom.enabled = false;
            arrowLeft.enabled = false;
            arrowRight.enabled = false;
        }

        public void Activate()
        {
            active = true;
        }

        public void Deactivate()
        {
            active = false;
        }
    }
}


﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ItemFinder.UI
{
    public class GameOverPopupController : PopupController
    {
        public Action PlayAgainPressed;
        public Action QuitPressed;

        [SerializeField] private Button playAgainButton;
        [SerializeField] private Button quitButton;
        [SerializeField] private TMP_Text scoreText;

        private void Start()
        {
            playAgainButton.onClick.AddListener(OnPlayAgainPressed);
            quitButton.onClick.AddListener(OnQuitPressed);
        }

        private void OnDestroy()
        {
            playAgainButton.onClick.RemoveListener(OnPlayAgainPressed);
            quitButton.onClick.RemoveListener(OnQuitPressed);
        }

        private void OnPlayAgainPressed()
        {
            Hide();
            PlayAgainPressed?.Invoke();
        }

        private void OnQuitPressed()
        {
            QuitPressed?.Invoke();
        }

        public void OnGameOver(int score)
        {
            scoreText.text = "Score: " + score.ToString();
            Show();
        }

    }
}